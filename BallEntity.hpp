#ifndef BALLENTITY_HPP_
#define BALLENTITY_HPP_

#include <SFML/Graphics.hpp>
#include "Entity.hpp"
#include "Scene.hpp"
#include "Game.hpp"
#include "CollisionType.hpp"
#include "MathUtils.hpp"
#include "json.hpp"

using Json = nlohmann::json;
using namespace std;
using namespace sf;

class BallEntity : public Entity
{
private:
    CircleShape m_shape;
    float m_speed;
    float m_direction;
    Vector2f m_velocity;
    FloatRect m_previousBounds;
    bool m_justCollided;
    Vector2f m_collidePosition;
    bool m_stopped;
    FloatRect m_bounds;

    void computeVelocity();
    void computeDirection();

public:
    struct CollisionData
    {
        public:
        float Distance;
        Vector2f IntersectionPoint;
        Vector2f Relative;
        FloatRect ObstacleBounds;
        CollisionType Type;

        Vector2f r1, r2, r3; // relative positions of corners, depending on center of the ball
        Vector2f a1, b1, c1; // real positions of the corners
        Vector2f a2, b2, c2; // predicted real positions of the corners
        float d1, d2, d3; // distances from the corners
        Vector2f i1, i2, i3; // intersection points from corners
        CollisionType t1, t2, t3; // collision types
    };

    static const int RADIUS = 5;
    static const int SPEED = 500;
    static constexpr const float MAX_BOUNCE_ANGLE = 17.0f * ((M_PI / 2.0f) / 18.0f);

    BallEntity(Scene& owner);
    BallEntity(Scene& owner, const Vector2f& initialPosition, bool stopped = true);

    virtual void initialize();
    virtual void processEvent(const Event& event);
    virtual void update(float delta);
    virtual void render();

    virtual const Vector2f& getPosition() const;
    virtual void setPosition(const Vector2f& value);
    bool isStopped() const;
    void start();
    float getSpeed() const;
    float getDirection() const;
    const Vector2f& getVelocity() const;
    void setSpeedDirection(float speed, float direction); 

    void bounceX();
    void bounceY();
    void bounce(CollisionType type);
    void bounce(CollisionType type, float directionChanger);

    void setCollidePosition(const Vector2f& value);
    Vector2f simulateNextPosition(float delta) const;
    void checkCollision(const FloatRect& bounds, float delta, CollisionData& data);
    void processCollision(const CollisionData& data, float& remainingDelta, CollisionType& previousType, bool paletteBounce);

    float getRadius() const;
    const FloatRect& getBounds();

    virtual Json toJson() const;
    virtual void loadJson(const Json& json);
};

#endif // BALLENTITY_HPP_