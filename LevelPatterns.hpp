#ifndef LEVELPATTERNS_HPP_
#define LEVELPATTERNS_HPP_

#include <vector>
#include <memory>
#include <string>
#include <SFML/Graphics.hpp>

using namespace std;
using namespace sf;

class LevelPatterns
{
private:
    LevelPatterns();

public:
    static void loadFromFile(const string& fileName, vector<FloatRect>& pattern);
    static void loadLevel(int level, vector<FloatRect>& pattern);
    static bool isLevelExists(int level);
    static string getLevelPath(int level);
};

#endif // LEVELPATTERNS_HPP_