#ifndef ENTITY_HPP_
#define ENTITY_HPP_

#include <memory>
#include <SFML/Window.hpp>
#include "json.hpp"
#include "JsonSerializable.hpp"

using Json = nlohmann::json;

using namespace std;
using namespace sf;

class Game;
class Scene;

class Entity : public JsonSerializable
{
private:
    Scene& m_owner;
    bool m_initialized;

public:
    Entity(Scene& owner);

    Scene& getScene();
    virtual void initialize();
    virtual const Vector2f& getPosition() const = 0;
    virtual void setPosition(const Vector2f& value) = 0;
    virtual void processEvent(const Event& event) = 0;
    virtual void update(float delta) = 0;
    virtual void render() = 0;

    bool isInitialized() const;
};

#endif // ENTITY_HPP_
