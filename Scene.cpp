#include "Scene.hpp"

Scene::Scene(Game& owner) :
    m_owner(owner),
    m_initialized(false)
{

}

Game& Scene::getGame()
{
    return m_owner;
}

void Scene::initialize()
{
    m_initialized = true;
}
bool Scene::isInitialized() const
{
    return m_initialized;
}