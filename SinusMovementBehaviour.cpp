#include "SinusMovementBehaviour.hpp"

#include <cmath>
#include "MathUtils.hpp"

const string SinusMovementBehaviour::UNIQUE_NAME("sinus");

SinusMovementBehaviour::SinusMovementBehaviour() :
    MovementBehaviour::MovementBehaviour(Vector2f(400, 50))
{
    setAmplitude(350.0f);
    setWidth(75.0f / M_PI);
    setProgressSpeed(M_PI / 8.0f);
}

float SinusMovementBehaviour::movementFunction(float x) const
{
    return sin(x);
}

const string& SinusMovementBehaviour::getUniqueName() const
{
    return SinusMovementBehaviour::UNIQUE_NAME;
}