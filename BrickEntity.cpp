#include "BrickEntity.hpp"

BrickEntity::BrickEntity(Scene& owner) :
    BrickEntity(owner, FloatRect(0, 0, 0, 0))
{
}
BrickEntity::BrickEntity(Scene& owner, const FloatRect& bounds, int durability) :
    BlockEntity::BlockEntity(owner, bounds),
    m_initialDurability(durability),
    m_currentDurability(durability)
{

}

void BrickEntity::initialize()
{
    BlockEntity::initialize();

    getShape().setFillColor(Color(64 + rand() % 128, 64 + rand() % 128, 64 + rand() % 128));
}

bool BrickEntity::isWrecked()
{
    return m_currentDurability <= 0;
}
void BrickEntity::decraseDurability(int value)
{
    m_currentDurability -= value;
    if (m_currentDurability < 0) m_currentDurability = 0;

    Color color = getShape().getFillColor();
    color.a = static_cast<Uint8>((static_cast<float>(m_currentDurability) / static_cast<float>(m_initialDurability)) * 256.0f);
    getShape().setFillColor(color);
}

Json BrickEntity::toJson() const
{
    Json json = BlockEntity::toJson();

    json["initialDurability"] = m_initialDurability;
    json["currentDurability"] = m_currentDurability;

    return json;
}
void BrickEntity::loadJson(const Json& json)
{
    BlockEntity::loadJson(json);

    m_initialDurability = json["initialDurability"];
    m_currentDurability = json["currentDurability"];
}