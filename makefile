CC=g++
LD=g++
CPPFLAGS=-c -Wall -std=c++11 -Iinclude -DSFML_STATIC -DWIN32_LEAN_AND_MEAN
DEBUGFLAGS=-g -DDEBUG
LINKERFLAGS=-Llib\SFML -static# -mwindows

PROJECTNAME=arkanoid
OBJDIR=obj
BINDIR=bin
DEBUGDIR=Debug
RELEASEDIR=Release
OBJ=main.o MathUtils.o Game.o Scene.o Entity.o LevelScene.o PaletteEntity.o BlockEntity.o BallEntity.o BrickEntity.o LevelPatterns.o BonusEntity.o EnemyEntity.o MovementBehaviour.o SinusMovementBehaviour.o CutTriangleMovementBehaviour.o DoubleTriangleMovementBehaviour.o NormalEnemyEntity.o BrickGeneratorEnemyEntity.o BonusGeneratorEnemyEntity.o AggregationVectorTest.o
DEPS=MathUtils.hpp Game.hpp Scene.hpp Entity.hpp LevelScene.hpp PaletteEntity.hpp BlockEntity.hpp BallEntity.cpp CollisionType.hpp BrickEntity.hpp LevelPatterns.hpp BonusEntity.hpp json.hpp EnemyEntity.hpp MovementBehaviour.hpp SinusMovementBehaviour.hpp CutTriangleMovementBehaviour.hpp DoubleTriangleMovementBehaviour.hpp JsonSerializable.hpp NormalEnemyEntity.hpp BrickGeneratorEnemyEntity.hpp BonusGeneratorEnemyEntity.hpp AggregationVector.hpp AggregationVectorTest.hpp
RELEASELIBS=sfml-main sfml-graphics-s sfml-window-s sfml-system-s gdi32 winmm opengl32 freetype jpeg
DEBUGLIBS=sfml-main-d sfml-graphics-s-d sfml-window-s-d sfml-system-s-d gdi32 winmm opengl32 freetype jpeg

DEBUGOBJ=$(patsubst %,$(OBJDIR)/$(DEBUGDIR)/%,$(OBJ))
RELEASEOBJ=$(patsubst %,$(OBJDIR)/$(RELEASEDIR)/%,$(OBJ))

RELEASELIBSARG=$(patsubst %,-l%,$(RELEASELIBS))
DEBUGLIBSARG=$(patsubst %,-l%,$(DEBUGLIBS))

all: release


release: $(RELEASEOBJ) | $(BINDIR)/$(RELEASEDIR)
	$(LD) $(LINKERFLAGS) $^ $(RELEASELIBSARG) -o $(BINDIR)/$(RELEASEDIR)/$(PROJECTNAME)
	cp levels $(BINDIR)/$(RELEASEDIR)/ -r

$(OBJDIR)/$(RELEASEDIR)/%.o: %.cpp $(DEPS) | $(OBJDIR)/$(RELEASEDIR)
	$(CC) $(CPPFLAGS) $< -o $@

$(OBJDIR)/$(RELEASEDIR):
	mkdir -p $@

$(BINDIR)/$(RELEASEDIR):
	mkdir -p $@


debug: $(DEBUGOBJ) | $(BINDIR)/$(DEBUGDIR)
	$(LD) $(LINKERFLAGS) $^ $(DEBUGLIBSARG) -o $(BINDIR)/$(DEBUGDIR)/$(PROJECTNAME)
	cp levels $(BINDIR)/$(DEBUGDIR)/ -r

$(OBJDIR)/$(DEBUGDIR)/%.o: %.cpp $(DEPS) | $(OBJDIR)/$(DEBUGDIR)
	$(CC) $(CPPFLAGS) $(DEBUGFLAGS) $< -o $@

$(OBJDIR)/$(DEBUGDIR):
	mkdir -p $@

$(BINDIR)/$(DEBUGDIR):
	mkdir -p $@

clean:
	rm -r -f $(OBJDIR)
	rm -r -f $(BINDIR)

