#include "EnemyEntity.hpp"

EnemyEntity::EnemyEntity(Scene& owner, shared_ptr<MovementBehaviour> behaviour, int size) :
    Entity::Entity(owner),
    m_behaviour(behaviour),
    m_size(size)
{
}

void EnemyEntity::initialize()
{
    m_health = 1.0f;
    setSize(m_size);   
    m_stopped = true;
    if (m_behaviour != nullptr) setPosition(m_behaviour->getPosition());

    m_shape.setFillColor(Color::Blue);
}
void EnemyEntity::processEvent(const Event& event)
{
    if (event.type == Event::MouseButtonPressed)
    {
        if (event.mouseButton.button == Mouse::Button::Left) start();
    }
}
void EnemyEntity::update(float delta)
{
    if (m_behaviour != nullptr && !m_stopped)
    {
        m_behaviour->update(delta);
        setPosition(m_behaviour->getPosition());
    }
}
void EnemyEntity::render()
{
    getScene().getGame().getWindow().draw(m_shape);
}

const Vector2f& EnemyEntity::getPosition() const
{
    return m_shape.getPosition();
}
void EnemyEntity::setPosition(const Vector2f& value)
{
    m_shape.setPosition(value);
}
void EnemyEntity::setHealth(float value)
{
    m_health = value;
}
float EnemyEntity::getHealth() const
{
    return m_health;
}
void EnemyEntity::setSize(int value)
{
    m_size = value;
    Vector2f vsize(m_size, m_size);
    m_shape.setSize(vsize);
    m_shape.setOrigin(vsize / 2.0f);
}
int EnemyEntity::getSize() const
{
    return m_size;
}
bool EnemyEntity::isAlive() const
{
    return m_health > 0.0f;
}
const FloatRect& EnemyEntity::getBounds()
{
    m_bounds = m_shape.getGlobalBounds();
    return m_bounds;
}
bool EnemyEntity::isStopped() const
{
    return m_stopped;
}
void EnemyEntity::start()
{
    m_stopped = false;
}
shared_ptr<MovementBehaviour> EnemyEntity::getMovementBehaviour()
{
    return m_behaviour;
}
void EnemyEntity::setMovementBehaviour(shared_ptr<MovementBehaviour> value)
{
    m_behaviour = value;
    if (m_behaviour != nullptr) setPosition(m_behaviour->getPosition());
}

Json EnemyEntity::toJson() const
{
    Json json;

    json["health"] = m_health;
    json["behaviour"] = m_behaviour->toJson();
    json["size"] = m_size;
    json["stopped"] = m_stopped;
    json["type"] = getUniqueName();

    return json;
}
void EnemyEntity::loadJson(const Json& json)
{
    m_health = json["health"];
    setSize(json["size"]);
    m_stopped = json["stopped"];
    Json behaviour = json["behaviour"];
    string type = behaviour["type"];
    if (type == SinusMovementBehaviour::UNIQUE_NAME) m_behaviour = make_shared<SinusMovementBehaviour>();
    else if (type == CutTriangleMovementBehaviour::UNIQUE_NAME) m_behaviour = make_shared<CutTriangleMovementBehaviour>();
    else if (type == DoubleTriangleMovementBehaviour::UNIQUE_NAME) m_behaviour = make_shared<DoubleTriangleMovementBehaviour>();
    if (m_behaviour != nullptr)
    {
        m_behaviour->loadJson(behaviour);
        setPosition(m_behaviour->getPosition());
    }
}