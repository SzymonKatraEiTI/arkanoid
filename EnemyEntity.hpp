#ifndef ENEMYENTITY_HPP_
#define ENEMYENTITY_HPP_

#include <SFML/Graphics.hpp>
#include "Entity.hpp"
#include "Scene.hpp"
#include "Game.hpp"
#include "json.hpp"
#include "MathUtils.hpp"
#include "MovementBehaviour.hpp"
#include "SinusMovementBehaviour.hpp"
#include "CutTriangleMovementBehaviour.hpp"
#include "DoubleTriangleMovementBehaviour.hpp"

using Json = nlohmann::json;
using namespace std;
using namespace sf;

class EnemyEntity : public Entity
{
    private:
    float m_health;
    shared_ptr<MovementBehaviour> m_behaviour;
    FloatRect m_bounds;
    int m_size;
    bool m_stopped;

    protected:
    RectangleShape m_shape;

    public:
    static const int DEFAULT_SIZE = 30;

    EnemyEntity(Scene& owner, shared_ptr<MovementBehaviour> behaviour = nullptr, int size = DEFAULT_SIZE);

    virtual void initialize();
    virtual void processEvent(const Event& event);
    virtual void update(float delta);
    virtual void render();

    virtual const Vector2f& getPosition() const;
    virtual void setPosition(const Vector2f& value);
    void setHealth(float value);
    float getHealth() const;
    bool isAlive() const;
    void setSize(int value);
    int getSize() const;
    const FloatRect& getBounds();
    bool isStopped() const;
    void start();
    shared_ptr<MovementBehaviour> getMovementBehaviour();
    void setMovementBehaviour(shared_ptr<MovementBehaviour> value);

    virtual Json toJson() const;
    virtual void loadJson(const Json& json);

    virtual const string& getUniqueName() const = 0;
};

#endif // ENEMYENTITY_HPP_