#ifndef BONUSGENERATORENEMYENTITY_HPP_
#define BONUSGENERATORENEMYENTITY_HPP_

#include <SFML/Graphics.hpp>
#include "EnemyEntity.hpp"
#include "Entity.hpp"
#include "Scene.hpp"
#include "Game.hpp"
#include "json.hpp"
#include "MathUtils.hpp"
#include "MovementBehaviour.hpp"

using Json = nlohmann::json;
using namespace std;
using namespace sf;

class BonusGeneratorEnemyEntity : public EnemyEntity
{
    private:
    float m_newBonusChance;
    float m_newBonusInterval;
    float m_newBonusIntervalCounter;    

    public:
    static const string UNIQUE_NAME;
    static constexpr float DEFAULT_NEW_BONUS_CHANCE = 0.002f;
    static constexpr float DEFAULT_NEW_BONUS_INTERVAL = 1.0f;

    BonusGeneratorEnemyEntity(Scene& owner, shared_ptr<MovementBehaviour> behaviour = nullptr, int size = EnemyEntity::DEFAULT_SIZE);

    virtual void initialize();
    virtual void update(float delta);

    bool newBonusRandom();

    float getNewBonusChance() const;
    void setNewBonusChance(float value);
    float getNewBonusInterval() const;
    void setNewBonusInterval(float value);

    virtual const string& getUniqueName() const;

    virtual Json toJson() const;
    virtual void loadJson(const Json& json);
};

#endif // BONUSGENERATORENEMYENTITY_HPP_