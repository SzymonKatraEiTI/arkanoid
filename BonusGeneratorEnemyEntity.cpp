#include "BonusGeneratorEnemyEntity.hpp"

#include <cstdlib>

const string BonusGeneratorEnemyEntity::UNIQUE_NAME("bonusgenerator");

BonusGeneratorEnemyEntity::BonusGeneratorEnemyEntity(Scene& owner, shared_ptr<MovementBehaviour> behaviour, int size) :
    EnemyEntity::EnemyEntity(owner, behaviour, size)
{
}

void BonusGeneratorEnemyEntity::initialize()
{
    EnemyEntity::initialize();

    m_newBonusInterval = DEFAULT_NEW_BONUS_INTERVAL;
    m_newBonusChance = DEFAULT_NEW_BONUS_CHANCE;
    m_newBonusIntervalCounter = m_newBonusInterval;

    m_shape.setFillColor(Color::Red);
}
void BonusGeneratorEnemyEntity::update(float delta)
{
    EnemyEntity::update(delta);

    m_newBonusIntervalCounter -= m_newBonusInterval * delta;
}

bool BonusGeneratorEnemyEntity::newBonusRandom()
{
    if (isStopped()) return false;
    if (m_newBonusIntervalCounter >= 0.0f) return false;

    if (MathUtils::RandomFloat() <= m_newBonusChance)
    {
        m_newBonusInterval = m_newBonusInterval;
        return true;
    }

    return false;
}

float BonusGeneratorEnemyEntity::getNewBonusChance() const
{
    return m_newBonusChance;
}
void BonusGeneratorEnemyEntity::setNewBonusChance(float value)
{
    m_newBonusChance = value;
}
float BonusGeneratorEnemyEntity::getNewBonusInterval() const
{
    return m_newBonusInterval;
}
void BonusGeneratorEnemyEntity::setNewBonusInterval(float value)
{
    m_newBonusInterval = value;
}

const string& BonusGeneratorEnemyEntity::getUniqueName() const
{
    return BonusGeneratorEnemyEntity::UNIQUE_NAME;
}

Json BonusGeneratorEnemyEntity::toJson() const
{
    Json json = EnemyEntity::toJson();

    json["newBonusChance"] = m_newBonusChance;
    json["newBonusInterval"] = m_newBonusInterval;
    json["newBonusIntervalCounter"] = m_newBonusIntervalCounter;

    return json;
}
void BonusGeneratorEnemyEntity::loadJson(const Json& json)
{
    EnemyEntity::loadJson(json);

    m_newBonusChance = json["newBonusChance"];
    m_newBonusInterval = json["newBonusInterval"];
    m_newBonusIntervalCounter = json["newBonusIntervalCounter"];
}