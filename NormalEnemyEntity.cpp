#include "NormalEnemyEntity.hpp"

const string NormalEnemyEntity::UNIQUE_NAME("normal");

NormalEnemyEntity::NormalEnemyEntity(Scene& scene, shared_ptr<MovementBehaviour> behaviour, int size) :
    EnemyEntity(scene, behaviour, size)
{

}

const string& NormalEnemyEntity::getUniqueName() const
{
    return NormalEnemyEntity::UNIQUE_NAME;
}