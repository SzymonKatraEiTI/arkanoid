#include "MathUtils.hpp"

#include <cmath>
#include <cfloat>
#include <cstdlib>

using namespace std;

bool MathUtils::LineIntersects(const Vector2f& a1, const Vector2f& a2, const Vector2f& b1, const Vector2f& b2, Vector2f& intersection)
{
    Vector2f b = a2 - a1;
    Vector2f d = b2 - b1;
    float bDotDPerp = b.x * d.y - b.y * d.x;

    // if b dot d == 0, it means the lines are parallel so have infinite intersection points
    if (bDotDPerp == 0)
        return false;

    Vector2f c = b1 - a1;
    float t = (c.x * d.y - c.y * d.x) / bDotDPerp;
    if (t < 0 || t > 1)
        return false;

    float u = (c.x * b.y - c.y * b.x) / bDotDPerp;
    if (u < 0 || u > 1)
        return false;

    intersection = a1 + t * b;

    return true;
}

CollisionType MathUtils::LineRectangleCollision(const Vector2f& a, const Vector2f& b, const FloatRect& rect, float& distance, Vector2f& intersection)
{
    Vector2f interPoint;
    distance = FLT_MAX;
    CollisionType type = CollisionType::None;

    Vector2f t1, t2;
    t1.x = rect.left;
    t1.y = rect.top;
    t2.x = rect.left + rect.width;
    t2.y = rect.top;

    if (MathUtils::LineIntersects(a, b, t1, t2, interPoint))
    {
        float dist = MathUtils::Distance(a, interPoint);
        if (dist < distance)
        {
            intersection = interPoint;
            distance = dist;
            type = CollisionType::Top;
        }
    }

    t1.x = t2.x;
    t2.y = rect.top + rect.height;

    if (MathUtils::LineIntersects(a, b, t1, t2, interPoint))
    {
        float dist = MathUtils::Distance(a, interPoint);
        if (dist < distance)
        {
            intersection = interPoint;
            distance = dist;
            type = CollisionType::Right;
        }
    }

    t1.y = t2.y;
    t2.x = rect.left;

    if (MathUtils::LineIntersects(a, b, t1, t2, interPoint))
    {
        float dist = MathUtils::Distance(a, interPoint);
        if (dist < distance)
        {
            intersection = interPoint;
            distance = dist;
            type = CollisionType::Bottom;
        }
    }

    t1.x = t2.x;
    t2.y = rect.top;

    if (MathUtils::LineIntersects(a, b, t1, t2, interPoint))
    {
        float dist = MathUtils::Distance(a, interPoint);
        if (dist < distance)
        {
            intersection = interPoint;
            distance = dist;
            type = CollisionType::Left;
        }
    }

    return type;
}

float MathUtils::Distance(const Vector2f& a, const Vector2f& b)
{
    float x = a.x - b.x;
    float y = a.y - b.y;
    return sqrt(x * x + y * y);
}
float MathUtils::RandomFloat()
{
    return static_cast<float>(rand() % 10000) / 10000.0f;
}