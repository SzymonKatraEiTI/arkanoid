#include <memory>
#include "Game.hpp"
#include "LevelScene.hpp"
#include "MathUtils.hpp"
#include "LevelPatterns.hpp"
#include <iostream>
#include <ctime>
#include <cstdlib>
#include "AggregationVectorTest.hpp"

using namespace std;

int main()
{
    AggregationVectorTest::runAll();

    srand(time(NULL));

    shared_ptr<Game> game = make_shared<Game>();
    shared_ptr<LevelScene> scene = make_shared<LevelScene>(*game, 0);
    game->setScene(scene);
    game->run();

    return 0;
}
