#ifndef MATHUTILS_HPP_
#define MATHUTILS_HPP_

#include <SFML/Graphics.hpp>
#include "CollisionType.hpp"

using namespace sf;

#define M_PI 3.14159265358979323846

class MathUtils
{
private:
    MathUtils();
public:
    static bool LineIntersects(const Vector2f& a1, const Vector2f& a2, const Vector2f& b1, const Vector2f& b2, Vector2f& intersection);
    static CollisionType LineRectangleCollision(const Vector2f& a, const Vector2f& b, const FloatRect& rect, float& distance, Vector2f& intersection);
    static float Distance(const Vector2f& a, const Vector2f& b);
    static float RandomFloat();
};

#endif // MATHUTILS_HPP_