#ifndef BRICKENTITY_HPP_
#define BRICKENTITY_HPP_

#include <SFML/Graphics.hpp>
#include "Scene.hpp"
#include "BlockEntity.hpp"
#include "json.hpp"

using Json = nlohmann::json;
using namespace std;
using namespace sf;

class BrickEntity : public BlockEntity
{
private:
    int m_initialDurability;
    int m_currentDurability;

public:
    static const int DEFAULT_WIDTH = 80;
    static const int DEFAULT_HEIGHT = 40;

    BrickEntity(Scene& owner);
    BrickEntity(Scene& owner, const FloatRect& bounds, int durability = 1);

    virtual void initialize();
    
    bool isWrecked();
    void decraseDurability(int value = 1);

    virtual Json toJson() const;
    virtual void loadJson(const Json& json);
};

#endif // BRICKENTITY_HPP_