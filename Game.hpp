#ifndef GAME_HPP_
#define GAME_HPP_

#include <memory>
#include <SFML/Graphics.hpp>
#include "Scene.hpp"

using namespace std;
using namespace sf;

class Game
{
private:
    RenderWindow m_window;
    shared_ptr<Scene> m_scene;
    shared_ptr<Scene> m_newScene;

public:
    static const int WIDTH = 800;
    static const int HEIGHT = 600;
    static const int MARGIN = 20;

    Game();
    void run();

    void setScene(shared_ptr<Scene> scene);
    shared_ptr<Scene> getScene();
    RenderWindow& getWindow();
};

#endif // GAME_HPP_
