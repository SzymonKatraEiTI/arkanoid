#include "LevelScene.hpp"

#include <algorithm>
#include <memory>
#include "MathUtils.hpp"
#include "LevelPatterns.hpp"
#include <fstream>
#include <cfloat>
#include "json.hpp"
#include <windows.h>
#include "SinusMovementBehaviour.hpp"
#include "CutTriangleMovementBehaviour.hpp"
#include "DoubleTriangleMovementBehaviour.hpp"
#include "NormalEnemyEntity.hpp"
#include "BrickGeneratorEnemyEntity.hpp"
#include "BonusGeneratorEnemyEntity.hpp"
#include <tuple>

using Json = nlohmann::json;

LevelScene::LevelScene(Game& owner, int level, int chances)
    : Scene::Scene(owner),
    m_level(level),
    m_chances(chances)
{
}

void LevelScene::initialize()
{
    Scene::initialize();

    m_paused = false;

    m_remainingBulletVisibilityTime = 0;
    m_leftBullet.setFillColor(Color::Yellow);
    m_rightBullet.setFillColor(Color::Yellow);

    m_palette = make_shared<PaletteEntity>(*this);
    m_palette->initialize();

    shared_ptr<BlockEntity> block;
    block = make_shared<BlockEntity>(*this, FloatRect(Game::MARGIN, 0, Game::WIDTH - Game::MARGIN, Game::MARGIN));
    m_walls.push_back(block);
    block = make_shared<BlockEntity>(*this, FloatRect(0, 0, Game::MARGIN, Game::HEIGHT));
    m_walls.push_back(block);
    block = make_shared<BlockEntity>(*this, FloatRect(Game::WIDTH - Game::MARGIN, 0, Game::MARGIN, Game::HEIGHT));
    m_walls.push_back(block);
    for (shared_ptr<BlockEntity>& i : m_walls) i->initialize();

    shared_ptr<BallEntity> ball = make_shared<BallEntity>(*this, Vector2f(m_palette->getPosition().x + m_palette->getBounds().width / 2, m_palette->getPosition().y));
    ball->initialize();
    ball->setPosition(ball->getPosition() - Vector2f(0, ball->getRadius()));
    m_balls.push_back(ball);

    vector<FloatRect> pattern;
    LevelPatterns::loadLevel(m_level, pattern);
    for (const FloatRect& i : pattern) m_bricks.push_back(make_shared<BrickEntity>(*this, i, 1));
    for (shared_ptr<BrickEntity>& i : m_bricks) i->initialize();

    m_chancesLeftFont.loadFromFile("C:\\Windows\\Fonts\\Arial.ttf");
    m_chancesLeftText.setFont(m_chancesLeftFont);
    m_chancesLeftText.setCharacterSize(36);
    m_chancesLeftText.setFillColor(Color::Yellow);
    m_chancesLeftText.setString(to_string(m_chances));

    int enemyCounter = 0;
    float speedMultiplier = 1.0f;
    float brickGenerateChanceMultiplier = 1.0f;
    float bonusGenerateChanceMultiplier = 0.5f;
    for (int i = 0; i < m_level; i++)
    {
        shared_ptr<MovementBehaviour> behaviour;
        //if (enemyCounter == 0) behaviour = make_shared<SinusMovementBehaviour>();
        //else if (enemyCounter == 1) behaviour = make_shared<CutTriangleMovementBehaviour>();
        //else behaviour = make_shared<DoubleTriangleMovementBehaviour>();
        //behaviour->setProgressSpeed(behaviour->getProgressSpeed() * speedMultiplier);
        
        shared_ptr<EnemyEntity> enemy;
        shared_ptr<BrickGeneratorEnemyEntity> brickGeneratorEntity;
        shared_ptr<BonusGeneratorEnemyEntity> bonusGeneratorEntity;
        switch (i % 3)
        {
            case 0:
                enemy = make_shared<NormalEnemyEntity>(*this, behaviour);
                break;

            case 1:
                brickGeneratorEntity = make_shared<BrickGeneratorEnemyEntity>(*this, behaviour);
                brickGeneratorEntity->setNewBrickChance(brickGeneratorEntity->getNewBrickChance() * brickGenerateChanceMultiplier);
                enemy = brickGeneratorEntity;
                break;
            
            case 2:
                bonusGeneratorEntity = make_shared<BonusGeneratorEnemyEntity>(*this, behaviour);
                bonusGeneratorEntity->setNewBonusChance(bonusGeneratorEntity->getNewBonusChance() * bonusGenerateChanceMultiplier);
                enemy = bonusGeneratorEntity;
                break;
        }

        enemy->initialize();
        m_enemies.push_back(enemy);

        if (++enemyCounter >= 3)
        {
            enemyCounter = 0;
            speedMultiplier += 0.15f;
            brickGenerateChanceMultiplier += 0.75f;
            bonusGenerateChanceMultiplier += 0.25f;
        }
    }

    size_t enemiesWithBehaviour = 0;
    int iter = 0;
    while (enemiesWithBehaviour < m_enemies.size())
    {
        tuple<shared_ptr<NormalEnemyEntity>,
              shared_ptr<BrickGeneratorEnemyEntity>,
              shared_ptr<BonusGeneratorEnemyEntity>> tup = m_enemies.get<NormalEnemyEntity, BrickGeneratorEnemyEntity, BonusGeneratorEnemyEntity>(iter);

        if (iter % 3 == 0)
        {
            if (changeEnemyBehaviour(get<0>(tup), make_shared<CutTriangleMovementBehaviour>(), 0, 0.03f)) enemiesWithBehaviour++;
            if (changeEnemyBehaviour(get<1>(tup), make_shared<CutTriangleMovementBehaviour>(), 1, 0.03f)) enemiesWithBehaviour++;
            if (changeEnemyBehaviour(get<2>(tup), make_shared<CutTriangleMovementBehaviour>(), 2, 0.03f)) enemiesWithBehaviour++;
        }
        else if (iter % 3 == 1)
        {
            if (changeEnemyBehaviour(get<0>(tup), make_shared<DoubleTriangleMovementBehaviour>(), 0, 0.03f)) enemiesWithBehaviour++;
            if (changeEnemyBehaviour(get<1>(tup), make_shared<DoubleTriangleMovementBehaviour>(), 1, 0.03f)) enemiesWithBehaviour++;
            if (changeEnemyBehaviour(get<2>(tup), make_shared<DoubleTriangleMovementBehaviour>(), 2, 0.03f)) enemiesWithBehaviour++;
        }
        else if (iter % 3 == 2)
        {
            if (changeEnemyBehaviour(get<0>(tup), make_shared<SinusMovementBehaviour>(), 0, M_PI / 16.0f)) enemiesWithBehaviour++;
            if (changeEnemyBehaviour(get<1>(tup), make_shared<SinusMovementBehaviour>(), 1, M_PI / 16.0f)) enemiesWithBehaviour++;
            if (changeEnemyBehaviour(get<2>(tup), make_shared<SinusMovementBehaviour>(), 2, M_PI / 16.0f)) enemiesWithBehaviour++;
        }

        iter++;
    }  
}

void LevelScene::processEvent(const Event& event)
{
    if (event.type == Event::KeyPressed)
    {
        if (event.key.code == Keyboard::R) getGame().setScene(make_shared<LevelScene>(getGame(), m_level, m_chances));
        else if (event.key.code == Keyboard::N && LevelPatterns::isLevelExists(m_level + 1)) getGame().setScene(make_shared<LevelScene>(getGame(), m_level + 1, m_chances));
        else if (event.key.code == Keyboard::S) saveStateToFile("savegame.json");
        else if (event.key.code == Keyboard::L) loadStateFromFile("savegame.json");
        else if (event.key.code == Keyboard::P)
        {
            m_paused = !m_paused;
            m_chancesLeftText.setString(m_paused ? "PAUZA" : to_string(m_chances));
        }
    }

    m_palette->processEvent(event);

    if (m_palette->areCannonsAvailable())
    {
        if (event.type == Event::MouseButtonPressed && event.mouseButton.button == Mouse::Button::Left)
        {
            m_palette->applyCannonsCooldown();
            checkBulletsCollisions();
        }
    }

    for (shared_ptr<BlockEntity>& i : m_walls) i->processEvent(event);
    for (shared_ptr<BrickEntity>& i : m_bricks) i->processEvent(event);
    for (shared_ptr<BonusEntity>& i : m_bonuses) i->processEvent(event);
    for (shared_ptr<BallEntity>& i : m_balls) i->processEvent(event);
    for (shared_ptr<EnemyEntity>& i : m_enemies) i->processEvent(event);
}
void LevelScene::update(float delta)
{
    if (m_paused) return;

    m_palette->update(delta);

    for (shared_ptr<BlockEntity>& i : m_walls) i->update(delta);
    for (shared_ptr<BrickEntity>& i : m_bricks) i->update(delta);
    processEnemys(delta);

    checkBonusesCollisions(delta);

    if (m_balls.size() > 0)
    {
        shared_ptr<BallEntity> ball = m_balls[0];
        if (ball->isStopped()) ball->setPosition(m_palette->getPosition() + Vector2f(m_palette->getBounds().width / 2.0f, -ball->getRadius()));
    }  

    vector<shared_ptr<BallEntity>> ballsToRemove;
    for (shared_ptr<BallEntity>& i : m_balls)
    {
        checkBallCollisions(delta, i);

        if (i->getPosition().y + BallEntity::RADIUS > Game::HEIGHT ||
            i->getPosition().y - BallEntity::RADIUS < 0 ||
            i->getPosition().x - BallEntity::RADIUS < 0 ||
            i->getPosition().x + BallEntity::RADIUS > Game::WIDTH) ballsToRemove.push_back(i);
    }
    for (shared_ptr<BallEntity>& i : ballsToRemove)
    {
        vector<shared_ptr<BallEntity>>::iterator x = find(m_balls.begin(), m_balls.end(), i);
        if (x != m_balls.end()) m_balls.erase(x);
    }

    if (m_remainingBulletVisibilityTime > 0) m_remainingBulletVisibilityTime -= delta;

    if (canProceedToNextLevel()) getGame().setScene(make_shared<LevelScene>(getGame(), m_level + 1, m_chances));

    if (isGameLose()) getGame().setScene(make_shared<LevelScene>(getGame(), m_level, m_chances - 1));
}
void LevelScene::render()
{ 
    m_palette->render();

    for (shared_ptr<BlockEntity>& i : m_walls) i->render();
    for (shared_ptr<BrickEntity>& i : m_bricks) i->render();
    for (shared_ptr<BonusEntity>& i : m_bonuses) i->render();
    for (shared_ptr<EnemyEntity>& i : m_enemies) i->render();
    for (shared_ptr<BallEntity>& i : m_balls) i->render();

    if (m_remainingBulletVisibilityTime > 0)
    {
        getGame().getWindow().draw(m_leftBullet);
        getGame().getWindow().draw(m_rightBullet);
    }

    getGame().getWindow().draw(m_chancesLeftText);
}

bool LevelScene::changeEnemyBehaviour(shared_ptr<EnemyEntity> enemy, shared_ptr<MovementBehaviour> behaviour, int index, float progressMultiplier)
{
    if (enemy == nullptr) return false;
    behaviour->setProgress(static_cast<float>(index) * progressMultiplier);
    enemy->setMovementBehaviour(behaviour);
    return true;
}

void LevelScene::checkBallCollisions(float delta, shared_ptr<BallEntity> ball)
{
    float ballDelta = delta;
    shared_ptr<Entity> lastCollision;
    CollisionType previousType = CollisionType::None;
    while (ballDelta > 0)
    {     
        BallEntity::CollisionData closestData;
        closestData.Distance = FLT_MAX;
        closestData.Type = CollisionType::None;
        shared_ptr<Entity> closestCollision = nullptr;
        bool isClosestPalette = false;

        BallEntity::CollisionData data;

        if (lastCollision != m_palette)
        {
            if (!m_palette->getBounds().intersects(ball->getBounds())) // do not handle case where ball is inside palette, it causes problems
            {
                ball->checkCollision(m_palette->getBounds(), delta, data);
                if (data.Type != CollisionType::None && data.Distance < closestData.Distance)
                {
                    closestData = data;
                    closestCollision = m_palette;
                    isClosestPalette = true;
                }
            }
        }

        for (shared_ptr<BlockEntity>& i : m_walls)
        {
            if (lastCollision != i)
            {
                ball->checkCollision(i->getBounds(), delta, data);
                if (data.Type != CollisionType::None && data.Distance < closestData.Distance)
                {
                    closestData = data;
                    closestCollision = i;
                }    
            }
        }

        shared_ptr<BrickEntity> hitBrick = nullptr;
      
        for (shared_ptr<BrickEntity>& i : m_bricks)
        {
            if (lastCollision != i)
            {
                ball->checkCollision(i->getBounds(), delta, data);
                if (data.Type != CollisionType::None && data.Distance < closestData.Distance)
                {
                    closestData = data;
                    closestCollision = i;
                    hitBrick = i;
                }         
            }
        }
        
        if (closestData.Type != CollisionType::None)
        {
            ball->processCollision(closestData, ballDelta, previousType, isClosestPalette);
            lastCollision = closestCollision;
            if (hitBrick != nullptr)
            {
                hitBrick->decraseDurability();
                if (hitBrick->isWrecked()) handleRemovingBrick(hitBrick);
            }
            continue;
        }

        ball->update(ballDelta);
        ballDelta = 0;
    }
}
void LevelScene::checkBulletsCollisions()
{
    float closestDistance;
    Vector2f closestIntersectionPoint;

     m_remainingBulletVisibilityTime = LevelScene::BULLET_VISIBILITY_TIME;

    // left cannon
    Vector2f leftStart = m_palette->getPosition();
    Vector2f leftEnd(leftStart.x, 0);

    processBulletCollision(leftStart, leftEnd, closestDistance, closestIntersectionPoint);
    m_leftBullet.setPosition(closestIntersectionPoint);
    m_leftBullet.setSize(Vector2f(1, closestDistance));

    // right cannon
    Vector2f rightStart = m_palette->getPosition() + Vector2f(m_palette->getWidth(), 0);
    Vector2f rightEnd(rightStart.x, 0);

    processBulletCollision(rightStart, rightEnd, closestDistance, closestIntersectionPoint);
    m_rightBullet.setPosition(closestIntersectionPoint);
    m_rightBullet.setSize(Vector2f(1, closestDistance));
}
void LevelScene::processBulletCollision(const Vector2f& start, const Vector2f& end, float& closestDistance, Vector2f& closestIntersectionPoint)
{
    closestDistance = FLT_MAX;
    shared_ptr<BrickEntity> brick = nullptr;
    shared_ptr<EnemyEntity> enemy = nullptr;

    float distance;
    Vector2f intersectionPoint;

    for (shared_ptr<BlockEntity>& i : m_walls)
    {
        if (MathUtils::LineRectangleCollision(start, end, i->getBounds(), distance, intersectionPoint) != CollisionType::None)
        {
            if (distance < closestDistance)
            {
                closestDistance = distance;
                closestIntersectionPoint = intersectionPoint;
            }
        }
    }

    for (shared_ptr<BrickEntity>& i : m_bricks)
    {
        if (MathUtils::LineRectangleCollision(start, end, i->getBounds(), distance, intersectionPoint) != CollisionType::None)
        {
            if (distance < closestDistance)
            {
                closestDistance = distance;
                closestIntersectionPoint = intersectionPoint;
                brick = i;
            }
        }
    }

    for (shared_ptr<EnemyEntity>& i : m_enemies)
    {
        if (MathUtils::LineRectangleCollision(start, end, i->getBounds(), distance, intersectionPoint) != CollisionType::None)
        {
            if (distance < closestDistance)
            {
                closestDistance = distance;
                closestIntersectionPoint = intersectionPoint;
                brick = nullptr;
                enemy = i;
            }
        }
    }

    if (brick != nullptr)
    {
        brick->decraseDurability();
        if (brick->isWrecked()) handleRemovingBrick(brick);
    }

    if (enemy != nullptr)
    {
        enemy->setHealth(0);
        if (!enemy->isAlive()) handleRemovingEnemy(enemy);
    }
}
void LevelScene::checkBonusesCollisions(float delta)
{
    vector<shared_ptr<BonusEntity>> bonusesToRemove;
    vector<shared_ptr<BonusEntity>> catchedBonuses;
    for (shared_ptr<BonusEntity>& i : m_bonuses)
    {
        i->update(delta);
        if (i->getBounds().intersects(m_palette->getBounds()))
        {
            catchedBonuses.push_back(i);
            bonusesToRemove.push_back(i);
        } 
        else if (i->getPosition().y > Game::HEIGHT) bonusesToRemove.push_back(i); 
    }

    for (shared_ptr<BonusEntity>& i : catchedBonuses)
    {
        vector<shared_ptr<BallEntity>> newBalls;
        switch (i->getType())
        {
            case BonusEntity::Type::CollapsePalette:
                m_palette->setWidth(m_palette->getWidth() - BonusEntity::PALETTE_RESIZE_PIXELS);
                break;

            case BonusEntity::Type::ExpandPalette:
                m_palette->setWidth(m_palette->getWidth() + BonusEntity::PALETTE_RESIZE_PIXELS);
                break;

            case BonusEntity::Type::DuplicateBall:
                for (shared_ptr<BallEntity>& j : m_balls)
                {
                    shared_ptr<BallEntity> ball = make_shared<BallEntity>(*this, j->getPosition(), false);
                    ball->initialize();
                    ball->setSpeedDirection(j->getSpeed(), j->getDirection());
                    ball->bounceX();
                    newBalls.push_back(ball);
                }
                for (shared_ptr<BallEntity>& j : newBalls) m_balls.push_back(j);
                break;

            case BonusEntity::Type::Bullets:
                m_palette->setCannons(true);
                break;

            case BonusEntity::Type::Count: break;
        }
    }
    for (shared_ptr<BonusEntity>& i : bonusesToRemove)
    {
        vector<shared_ptr<BonusEntity>>::iterator x = find(m_bonuses.begin(), m_bonuses.end(), i);
        if (x != m_bonuses.end()) m_bonuses.erase(x);
    }
}
void LevelScene::handleRemovingBrick(shared_ptr<BrickEntity> brick)
{
    if (MathUtils::RandomFloat() < LevelScene::BONUS_CHANCE)
    {
        shared_ptr<BonusEntity> bonus = make_shared<BonusEntity>(*this);
        const FloatRect& bounds = brick->getBounds();
        Vector2f bonusPos(bounds.left + bounds.width / 2 - BonusEntity::SIZE / 2, bounds.top + bounds.height);
        bonus->setPosition(bonusPos);
        bonus->initialize();
        m_bonuses.push_back(bonus);
    }

    vector<shared_ptr<BrickEntity>>::iterator x = find(m_bricks.begin(), m_bricks.end(), brick);
    if (x != m_bricks.end()) m_bricks.erase(x);   
}
void LevelScene::handleRemovingEnemy(shared_ptr<EnemyEntity> enemy)
{
    vector<shared_ptr<EnemyEntity>>::iterator x = find(m_enemies.begin(), m_enemies.end(), enemy);
    if (x != m_enemies.end()) m_enemies.erase(x);
}
void LevelScene::processEnemys(float delta)
{
    vector<shared_ptr<EnemyEntity>> enemiesToRemove;

    for (shared_ptr<EnemyEntity>& enemy : m_enemies)
    {
        enemy->update(delta);

        if (shared_ptr<BrickGeneratorEnemyEntity> brickGenEnemy = dynamic_pointer_cast<BrickGeneratorEnemyEntity>(enemy))
        {
            if (brickGenEnemy->newBrickRandom())
            {
                const Vector2f& pos = brickGenEnemy->getPosition();
                shared_ptr<BrickEntity> brick = make_shared<BrickEntity>(*this, FloatRect(pos.x, pos.y, BrickEntity::DEFAULT_WIDTH, BrickEntity::DEFAULT_HEIGHT));
                brick->initialize();
                m_bricks.push_back(brick);
            }
        }
        else if (shared_ptr<BonusGeneratorEnemyEntity> bonusGenEnemy = dynamic_pointer_cast<BonusGeneratorEnemyEntity>(enemy))
        {
            if (bonusGenEnemy->newBonusRandom())
            {
                const Vector2f& pos = bonusGenEnemy->getPosition();
                shared_ptr<BonusEntity> bonus = make_shared<BonusEntity>(*this, BonusEntity::Type::CollapsePalette);
                bonus->setPosition(pos);
                bonus->initialize();
                m_bonuses.push_back(bonus);
            }
        }

        for (shared_ptr<BallEntity>& ball : m_balls)
        {
            if (ball->getBounds().intersects(enemy->getBounds()))
            {
                enemy->setHealth(0);
                if (!enemy->isAlive()) enemiesToRemove.push_back(enemy);
                break;
            }
        }
    }

    for (shared_ptr<EnemyEntity>& i : enemiesToRemove) handleRemovingEnemy(i);
}
bool LevelScene::canProceedToNextLevel() const
{
    return m_bricks.size() <= 0 && m_enemies.size() <= 0 && LevelPatterns::isLevelExists(m_level + 1);
}
bool LevelScene::isGameLose() const
{
    for (const shared_ptr<EnemyEntity>& enemy : m_enemies)
    {
        if (enemy->getPosition().y >= Game::HEIGHT) return true;
    }

    return m_balls.size() <= 0 && m_chances > 0;
}

void LevelScene::saveStateToFile(const string& fileName)
{
    try
    {
        Json root;
        root["level"] = m_level;
        root["chances"] = m_chances;

        root["palette"] = m_palette->toJson();

        Json jBalls;
        for (shared_ptr<BallEntity>& ball : m_balls) jBalls.push_back(ball->toJson());
        root["balls"] = jBalls;
    
        Json jWalls;
        for (shared_ptr<BlockEntity>& wall : m_walls) jWalls.push_back(wall->toJson());
        root["walls"] = jWalls;

        Json jBricks;
        for (shared_ptr<BrickEntity>& brick : m_bricks) jBricks.push_back(brick->toJson());
        root["bricks"] = jBricks;

        Json jBonuses;
        for (shared_ptr<BonusEntity>& bonus : m_bonuses) jBonuses.push_back(bonus->toJson());
        root["bonuses"] = jBonuses;

        Json jEnemys;
        for (shared_ptr<EnemyEntity>& enemy : m_enemies) jEnemys.push_back(enemy->toJson());
        root["enemys"] = jEnemys;

        fstream file(fileName, fstream::out | fstream::trunc);
        file << setw(4) << root;
        file.close();
    }
    catch (...)
    {
        string msg = "An error occured while saving game state to " + fileName;
        MessageBox(NULL, msg.c_str(), NULL, MB_OK | MB_ICONEXCLAMATION);
    }
}
void LevelScene::loadStateFromFile(const string& fileName)
{
    try
    {
        Json root;

        fstream file(fileName, fstream::in);
        file >> root;
        file.close();

        m_level = root["level"];
        m_chances = root["chances"];

        m_palette->loadJson(root["palette"]);

        m_balls.clear();
        for (Json& jBall : root["balls"])
        {
            shared_ptr<BallEntity> ball = make_shared<BallEntity>(*this);
            ball->initialize();
            ball->loadJson(jBall);
            m_balls.push_back(ball);
        }

        m_walls.clear();
        for (Json& jWall : root["walls"])
        {
            shared_ptr<BlockEntity> wall = make_shared<BlockEntity>(*this);
            wall->initialize();
            wall->loadJson(jWall);
            m_walls.push_back(wall);
        }

        m_bricks.clear();
        for (Json& jBrick : root["bricks"])
        {
            shared_ptr<BrickEntity> brick = make_shared<BrickEntity>(*this);
            brick->initialize();
            brick->loadJson(jBrick);
            m_bricks.push_back(brick);
        }

        m_bonuses.clear();
        for (Json& jBonus : root["bonuses"])
        {
            shared_ptr<BonusEntity> bonus = make_shared<BonusEntity>(*this);
            bonus->initialize();
            bonus->loadJson(jBonus);
            m_bonuses.push_back(bonus);
        }

        m_enemies.clear();
        for (Json& jEnemy : root["enemys"])
        {
            shared_ptr<EnemyEntity> enemy;

            string type = jEnemy["type"];
            if (type == NormalEnemyEntity::UNIQUE_NAME) enemy = make_shared<NormalEnemyEntity>(*this);
            else if (type == BrickGeneratorEnemyEntity::UNIQUE_NAME) enemy = make_shared<BrickGeneratorEnemyEntity>(*this);
            else if (type == BonusGeneratorEnemyEntity::UNIQUE_NAME) enemy = make_shared<BonusGeneratorEnemyEntity>(*this);

            if (enemy != nullptr)
            {
                enemy->initialize();
                enemy->loadJson(jEnemy);
                m_enemies.push_back(enemy);
            }
        }
    }
    catch (...)
    {
        string msg = "An error occured while loading game state from " + fileName;
        MessageBox(NULL, msg.c_str(), NULL, MB_OK | MB_ICONEXCLAMATION);
    }
}