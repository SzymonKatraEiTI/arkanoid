#ifndef JSONSERIALIZABLE_HPP_
#define JSONSERIALIZABLE_HPP_

#include "json.hpp"

using Json = nlohmann::json;

class JsonSerializable
{
    virtual Json toJson() const = 0;
    virtual void loadJson(const Json& json) = 0;
};

#endif // JSONSERIALIZABLE_HPP_