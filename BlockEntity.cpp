#include "BlockEntity.hpp"

BlockEntity::BlockEntity(Scene& owner) :
    BlockEntity(owner, FloatRect(0, 0, 0, 0))
{
}
BlockEntity::BlockEntity(Scene& owner, const FloatRect& bounds) :
    Entity::Entity(owner)
{
    m_bounds = bounds;
}

void BlockEntity::initialize()
{
    Entity::initialize();

    m_shape.setPosition(m_bounds.left, m_bounds.top);
    m_shape.setSize(Vector2f(m_bounds.width, m_bounds.height));
    m_shape.setFillColor(Color::Black);
}

void BlockEntity::processEvent(const Event& event)
{

}
void BlockEntity::update(float delta)
{

}
void BlockEntity::render()
{
    getScene().getGame().getWindow().draw(m_shape);
}

const FloatRect& BlockEntity::getBounds() const
{
    return m_bounds;
}

const Vector2f& BlockEntity::getPosition() const
{
    return m_shape.getPosition();
}
void BlockEntity::setPosition(const Vector2f& value)
{
    m_shape.setPosition(value);
    m_bounds.left = value.x;
    m_bounds.top = value.y;
}
void BlockEntity::setSize(const Vector2f& value)
{
    m_shape.setSize(value);
    m_bounds.width = value.x;
    m_bounds.height = value.y;
}

RectangleShape& BlockEntity::getShape()
{
    return m_shape;
}

Json BlockEntity::toJson() const
{
    Json json;

    Json jBounds;
    jBounds[0] = m_bounds.left;
    jBounds[1] = m_bounds.top;
    jBounds[2] = m_bounds.width;
    jBounds[3] = m_bounds.height;

    json["bounds"] = jBounds;

    Color color = m_shape.getFillColor();
    Json jColor;
    jColor[0] = color.r;
    jColor[1] = color.g;
    jColor[2] = color.b;
    jColor[3] = color.a;

    json["color"] = jColor;

    return json;
}
void BlockEntity::loadJson(const Json& json)
{
    Json jBounds = json["bounds"];
    setPosition(Vector2f(jBounds[0], jBounds[1]));
    setSize(Vector2f(jBounds[2], jBounds[3]));

    Json jColor = json["color"];
    m_shape.setFillColor(Color(jColor[0], jColor[1], jColor[2], jColor[3]));
}