#ifndef CUTTRIANGLEMOVEMENTBEHAVIOUR_HPP_
#define CUTTRIANGLEMOVEMENTBEHAVIOUR_HPP_

#include <SFML/Graphics.hpp>
#include "MovementBehaviour.hpp"

class CutTriangleMovementBehaviour : public MovementBehaviour
{
    protected:
    virtual float movementFunction(float x) const;

    public:
    CutTriangleMovementBehaviour();

    virtual const string& getUniqueName() const;

    static const string UNIQUE_NAME;
};

#endif // CUTTRIANGLEMOVEMENTBEHAVIOUR_HPP_