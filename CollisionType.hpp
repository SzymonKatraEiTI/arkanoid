#ifndef COLLISIONTYPE_HPP_
#define COLLISIONTYPE_HPP_

enum class CollisionType
{
    None,
    Left,
    Top,
    Right,
    Bottom
};

#endif // COLLISIONTYPE_HPP_