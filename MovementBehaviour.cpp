#include "MovementBehaviour.hpp"

MovementBehaviour::MovementBehaviour() :
    MovementBehaviour::MovementBehaviour(Vector2f(0, 0))
{
}
MovementBehaviour::MovementBehaviour(Vector2f offset) :
    m_progress(0.0f),
    m_offset(offset)
{
}

void MovementBehaviour::update(float delta)
{
    m_progress += m_progressSpeed * delta;
}
Vector2f MovementBehaviour::getPosition() const
{
    return Vector2f(movementFunction(m_progress) * m_amplitude, m_progress * m_width) + m_offset;
}
void MovementBehaviour::setOffset(Vector2f value)
{
    m_offset = value;
}
const Vector2f& MovementBehaviour::getOffset() const
{
    return m_offset;
}
void MovementBehaviour::setProgressSpeed(float value)
{
    m_progressSpeed = value;
}
float MovementBehaviour::getProgressSpeed() const
{
    return m_progressSpeed;
}
void MovementBehaviour::setAmplitude(float value)
{
    m_amplitude = value;
}
float MovementBehaviour::getAmplitude() const
{
    return m_amplitude;
}
void MovementBehaviour::setWidth(float value)
{
    m_width = value;
}
float MovementBehaviour::getWidth() const
{
    return m_width;
}
float MovementBehaviour::getProgress() const
{
    return m_progress;
}
void MovementBehaviour::setProgress(float value)
{
    m_progress = value;
}

Json MovementBehaviour::toJson() const
{
    Json json;

    json["progress"] = m_progress;
    json["progressSpeed"] = m_progressSpeed;
    json["amplitude"] = m_amplitude;
    json["width"] = m_width;
    json["offset"][0] = m_offset.x;
    json["offset"][1] = m_offset.y;
    json["type"] = getUniqueName();

    return json;
}
void MovementBehaviour::loadJson(const Json& json)
{
    m_progress = json["progress"];
    m_progressSpeed = json["progressSpeed"];
    m_amplitude = json["amplitude"];
    m_width = json["width"];
    m_offset.x = json["offset"][0];
    m_offset.y = json["offset"][1];
}