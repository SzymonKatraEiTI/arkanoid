#ifndef MOVEMENTBEHAVIOUR_HPP_
#define MOVEMENTBEHAVIOUR_HPP_

#include <SFML/Graphics.hpp>
#include "json.hpp"
#include "JsonSerializable.hpp"

using Json = nlohmann::json;

using namespace std;
using namespace sf;

class MovementBehaviour : public JsonSerializable
{
    private:
    float m_progress;
    float m_progressSpeed;
    float m_amplitude;
    float m_width;
    Vector2f m_offset;

    protected:
    virtual float movementFunction(float x) const = 0;

    public:
    MovementBehaviour();
    MovementBehaviour(Vector2f offset);

    void update(float delta);
    virtual Vector2f getPosition() const;
    void setOffset(Vector2f value);
    const Vector2f& getOffset() const;
    void setProgressSpeed(float value);
    float getProgressSpeed() const;
    void setAmplitude(float value);
    float getAmplitude() const;
    void setWidth(float value);
    float getWidth() const;
    float getProgress() const;
    void setProgress(float value);

    virtual Json toJson() const;
    virtual void loadJson(const Json& json);

    virtual const string& getUniqueName() const = 0;
};

#endif // MOVEMENTBEHAVIOUR_HPP_