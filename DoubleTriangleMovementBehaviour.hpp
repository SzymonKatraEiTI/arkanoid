#ifndef DOUBLETRIANGLEMOVEMENTBEHAVIOUR_HPP_
#define DOUBLETRIANGLEMOVEMENTBEHAVIOUR_HPP_

#include <SFML/Graphics.hpp>
#include "MovementBehaviour.hpp"

class DoubleTriangleMovementBehaviour : public MovementBehaviour
{
    protected:
    virtual float movementFunction(float x) const;

    public:
    DoubleTriangleMovementBehaviour();

    virtual const string& getUniqueName() const;

    static const string UNIQUE_NAME;
};

#endif // DOUBLETRIANGLEMOVEMENTBEHAVIOUR_HPP_