#include "PaletteEntity.hpp"

#include <SFML/Window.hpp>

RectangleShape PaletteEntity::s_cannon;
bool PaletteEntity::s_cannonInitialized = false;

PaletteEntity::PaletteEntity(Scene& owner)
    : Entity::Entity(owner)
{ 
}

void PaletteEntity::initialize()
{
    if (!s_cannonInitialized)
    {
        s_cannon.setPosition(Vector2f(0, 0));
        s_cannon.setSize(Vector2f(PaletteEntity::CANNON_WIDTH, PaletteEntity::HEIGHT));
        s_cannon.setFillColor(Color::Yellow);

        s_cannonInitialized = true;
    }

    Entity::initialize();

    m_hasCannons = false;
    m_cannonCooldownTime = 0;
    m_width = PaletteEntity::DEFAULT_WIDTH;
    m_shape.setPosition(Game::WIDTH / 2 - m_width / 2, Game::HEIGHT - PaletteEntity::HEIGHT - Game::MARGIN);
    m_shape.setSize(Vector2f(m_width, PaletteEntity::HEIGHT));
    m_shape.setFillColor(Color::Black);
}
const Vector2f& PaletteEntity::getPosition() const
{
    return m_shape.getPosition();
}
void PaletteEntity::setPosition(const Vector2f& value)
{
    m_shape.setPosition(value);
}

void PaletteEntity::processEvent(const Event& event)
{
    
}
void PaletteEntity::update(float delta)
{
    Vector2f pos = m_shape.getPosition();

    Vector2i mousePos = Mouse::getPosition(getScene().getGame().getWindow());
    pos.x = mousePos.x - m_width / 2;

    if (pos.x < Game::MARGIN) pos.x = Game::MARGIN;
    else if (pos.x > Game::WIDTH - Game::MARGIN - m_shape.getSize().x) pos.x = Game::WIDTH - Game::MARGIN - m_shape.getSize().x;

    m_shape.setPosition(pos);

    if (m_cannonCooldownTime > 0) m_cannonCooldownTime -= delta;
}
void PaletteEntity::render()
{
    getScene().getGame().getWindow().draw(m_shape);

    if (m_hasCannons)
    {
        Vector2f pos = m_shape.getPosition();

        Transform tran = Transform::Identity;
        m_leftCannonStates.transform = tran.translate(pos);

        tran = Transform::Identity;
        m_rightCannonStates.transform = tran.translate(pos).translate(m_width - PaletteEntity::CANNON_WIDTH, 0);

        getScene().getGame().getWindow().draw(s_cannon, m_leftCannonStates);
        getScene().getGame().getWindow().draw(s_cannon, m_rightCannonStates);        
    }
}

const FloatRect& PaletteEntity::getBounds()
{
    m_bounds = m_shape.getGlobalBounds();
    return m_bounds;
}

void PaletteEntity::setWidth(int value)
{
    m_width = value;
    if (m_width < MIN_WIDTH) m_width = MIN_WIDTH;
    else if (m_width > MAX_WIDTH) m_width = MAX_WIDTH;

    Vector2f size = m_shape.getSize();
    size.x = m_width;
    
    m_shape.setSize(size);
}
int PaletteEntity::getWidth() const
{
    return m_width;
}

void PaletteEntity::setCannons(bool value)
{
    m_hasCannons = value;
}
bool PaletteEntity::areCannonsAvailable() const
{
    return m_hasCannons && m_cannonCooldownTime <= 0;
}
bool PaletteEntity::hasCannons() const
{
    return m_hasCannons;
}
void PaletteEntity::applyCannonsCooldown()
{
    m_cannonCooldownTime = PaletteEntity::CANNON_COOLDOWN_TIME;
}

Json PaletteEntity::toJson() const
{
    const Vector2f& pos = getPosition(); 

    Json json;

    json["position"][0] = pos.x;
    json["position"][1] = pos.y;
    json["width"] = m_width;
    json["hasCannons"] = m_hasCannons;
    json["cannonCooldownTime"] = m_cannonCooldownTime;

    return json;
}
void PaletteEntity::loadJson(const Json& json)
{
    Vector2f pos(json["position"][0], json["position"][1]);

    setPosition(pos);
    setWidth(json["width"]);
    setCannons(json["hasCannons"]);
    m_cannonCooldownTime = json["cannonCooldownTime"];
}