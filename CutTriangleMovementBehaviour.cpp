#include "CutTriangleMovementBehaviour.hpp"

#include <cmath>
#include "MathUtils.hpp"

const string CutTriangleMovementBehaviour::UNIQUE_NAME("cuttriangle");

CutTriangleMovementBehaviour::CutTriangleMovementBehaviour() :
    MovementBehaviour::MovementBehaviour(Vector2f(400, 50))
{
    setAmplitude(350.0f);
    setWidth(75.0f);
    setProgressSpeed(0.18f);
}

float CutTriangleMovementBehaviour::movementFunction(float x) const
{
    double ipart;
    x = modf(x, &ipart);

    if (x > 0.8f) return 5.0f * x - 5.0f;
    else if (x > 0.7) return -1.0f;
    else if (x > 0.3) return -5.0f * x + 2.5f;
    else if (x > 0.2) return 1.0f;
    else return 5.0f * x;
}

const string& CutTriangleMovementBehaviour::getUniqueName() const
{
    return CutTriangleMovementBehaviour::UNIQUE_NAME;
}