#ifndef BLOCKENTITY_HPP_
#define BLOCKENTITY_HPP_

#include <memory>
#include <SFML/Graphics.hpp>
#include "Entity.hpp"
#include "Scene.hpp"
#include "Game.hpp"
#include "json.hpp"

using Json = nlohmann::json;
using namespace std;
using namespace sf;

class BlockEntity : public Entity
{
private:
    FloatRect m_bounds;
    RectangleShape m_shape;

protected:
    RectangleShape& getShape();

public:
    BlockEntity(Scene& owner);
    BlockEntity(Scene& owner, const FloatRect& bounds);

    virtual void initialize();
    virtual void processEvent(const Event& event);
    virtual void update(float delta);
    virtual void render();

    const FloatRect& getBounds() const;

    virtual const Vector2f& getPosition() const;
    virtual void setPosition(const Vector2f& value);
    void setSize(const Vector2f& value);

    virtual Json toJson() const;
    virtual void loadJson(const Json& json);
};

#endif // BLOCKENTITY_HPP_