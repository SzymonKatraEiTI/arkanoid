#include "DoubleTriangleMovementBehaviour.hpp"

#include <cmath>
#include "MathUtils.hpp"

const string DoubleTriangleMovementBehaviour::UNIQUE_NAME("doubletriangle");

DoubleTriangleMovementBehaviour::DoubleTriangleMovementBehaviour() :
    MovementBehaviour::MovementBehaviour(Vector2f(400, 50))
{
    setAmplitude(350.0f);
    setWidth(150.0f);
    setProgressSpeed(0.1f);
}

float DoubleTriangleMovementBehaviour::movementFunction(float x) const
{
    double ipart;
    x = modf(x, &ipart);

    if (x > 0.875f) return 8.0f * x - 8.0f;
    else if (x > 0.75f) return -8.0f * x + 6.0f;
    else if (x > 0.625f) return 8.0f * x - 6.0f;
    else if (x > 0.375f) return -8.0f * x + 4.0f;
    else if (x > 0.25f) return 8.0f * x - 2.0f;
    else if (x > 0.125f) return -8.0f * x + 2.0f;
    else return 8.0f * x;
}

const string& DoubleTriangleMovementBehaviour::getUniqueName() const
{
    return DoubleTriangleMovementBehaviour::UNIQUE_NAME;
}