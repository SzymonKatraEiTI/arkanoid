#include "BonusEntity.hpp"

#include "MathUtils.hpp"

VertexArray BonusEntity::s_expandPalettePattern;
VertexArray BonusEntity::s_collapsePalettePattern;
CircleShape BonusEntity::s_duplicateBallPattern1;
CircleShape BonusEntity::s_duplicateBallPattern2;
RectangleShape BonusEntity::s_bulletsPattern1;
RectangleShape BonusEntity::s_bulletsPattern2;
bool BonusEntity::s_patternsInitialized = false;

BonusEntity::BonusEntity(Scene& owner) :
    Entity::Entity(owner)
{
    float ran = MathUtils::RandomFloat();
    if (ran < BonusEntity::EXPAND_PALETTE_CHANCE)
    {
        m_type = Type::ExpandPalette;
    }
    else if (ran < BonusEntity::EXPAND_PALETTE_CHANCE + BonusEntity::COLLAPSE_PALETTE_CHANCE)
    {
        m_type = Type::CollapsePalette;
    }
    else if (ran < BonusEntity::EXPAND_PALETTE_CHANCE + BonusEntity::COLLAPSE_PALETTE_CHANCE + BonusEntity::DUPLICATE_BALL_CHANCE)
    {
        m_type = Type::DuplicateBall;
    }
    else m_type = Type::Bullets;
}
BonusEntity::BonusEntity(Scene& owner, Type type) :
    Entity::Entity(owner),
    m_type(type)
{
}

void BonusEntity::initialize()
{
    if (!s_patternsInitialized)
    {
        s_expandPalettePattern.setPrimitiveType(PrimitiveType::Triangles);

        s_expandPalettePattern.append(Vertex(Vector2f(5, 20), Color::Green));
        s_expandPalettePattern.append(Vertex(Vector2f(15, 10), Color::Green));
        s_expandPalettePattern.append(Vertex(Vector2f(15, 30), Color::Green));

        s_expandPalettePattern.append(Vertex(Vector2f(15, 15), Color::Green));
        s_expandPalettePattern.append(Vertex(Vector2f(25, 25), Color::Green));
        s_expandPalettePattern.append(Vertex(Vector2f(15, 25), Color::Green));

        s_expandPalettePattern.append(Vertex(Vector2f(15, 15), Color::Green));
        s_expandPalettePattern.append(Vertex(Vector2f(25, 15), Color::Green));
        s_expandPalettePattern.append(Vertex(Vector2f(25, 25), Color::Green));

        s_expandPalettePattern.append(Vertex(Vector2f(25, 10), Color::Green));
        s_expandPalettePattern.append(Vertex(Vector2f(35, 20), Color::Green));
        s_expandPalettePattern.append(Vertex(Vector2f(25, 30), Color::Green));

        s_collapsePalettePattern.setPrimitiveType(PrimitiveType::Triangles);

        s_collapsePalettePattern.append(Vertex(Vector2f(5, 10), Color::Red));
        s_collapsePalettePattern.append(Vertex(Vector2f(20, 20), Color::Red));
        s_collapsePalettePattern.append(Vertex(Vector2f(5, 30), Color::Red));

        s_collapsePalettePattern.append(Vertex(Vector2f(20, 20), Color::Red));
        s_collapsePalettePattern.append(Vertex(Vector2f(35, 10), Color::Red));
        s_collapsePalettePattern.append(Vertex(Vector2f(35, 30), Color::Red));

        s_duplicateBallPattern1.setFillColor(Color::Green);
        s_duplicateBallPattern1.setRadius(5);
        s_duplicateBallPattern1.setOrigin(Vector2f(5, 5));
        s_duplicateBallPattern1.setPosition(Vector2f(15, 15));

        s_duplicateBallPattern2.setFillColor(Color::Green);
        s_duplicateBallPattern2.setRadius(5);
        s_duplicateBallPattern2.setOrigin(Vector2f(5, 5));
        s_duplicateBallPattern2.setPosition(Vector2f(25, 25));

        s_bulletsPattern1.setFillColor(Color::Yellow);
        s_bulletsPattern1.setPosition(Vector2f(5, 10));
        s_bulletsPattern1.setSize(Vector2f(5, 20));

        s_bulletsPattern2.setFillColor(Color::Yellow);
        s_bulletsPattern2.setPosition(Vector2f(30, 10));
        s_bulletsPattern2.setSize(Vector2f(5, 20));

        s_patternsInitialized = true;
    }

    m_speed = rand() % (MAX_SPEED - MIN_SPEED) + MIN_SPEED;
    m_backShape.setSize(Vector2f(SIZE, SIZE));
    m_backShape.setFillColor(Color(192, 192, 192));
}
const Vector2f& BonusEntity::getPosition() const
{
    return m_backShape.getPosition();
}
void BonusEntity::setPosition(const Vector2f& value)
{
    m_backShape.setPosition(value);
}
void BonusEntity::processEvent(const Event& event)
{

}
void BonusEntity::update(float delta)
{
    Vector2f pos = m_backShape.getPosition();
    pos.y += delta * m_speed;
    m_backShape.setPosition(pos);
}
void BonusEntity::render()
{
    Vector2f pos = m_backShape.getPosition();
    Transform tran = Transform::Identity;
    m_states.transform = tran.translate(pos);

    getScene().getGame().getWindow().draw(m_backShape);
    switch (m_type)
    {
        case Type::CollapsePalette:
            getScene().getGame().getWindow().draw(s_collapsePalettePattern, m_states);
            break;

        case Type::ExpandPalette:
            getScene().getGame().getWindow().draw(s_expandPalettePattern, m_states);
            break;

        case Type::DuplicateBall:
            getScene().getGame().getWindow().draw(s_duplicateBallPattern1, m_states);
            getScene().getGame().getWindow().draw(s_duplicateBallPattern2, m_states);
            break;

        case Type::Bullets:
            getScene().getGame().getWindow().draw(s_bulletsPattern1, m_states);
            getScene().getGame().getWindow().draw(s_bulletsPattern2, m_states);
            break;

        case Type::Count: break;
    }
}

const FloatRect& BonusEntity::getBounds()
{
    m_bounds = m_backShape.getGlobalBounds();
    return m_bounds;
}

BonusEntity::Type BonusEntity::getType() const
{
    return m_type;
}

Json BonusEntity::toJson() const
{
    const Vector2f& pos = getPosition();

    Json json;

    json["position"][0] = pos.x;
    json["position"][1] = pos.y;
    json["type"] = static_cast<int>(m_type);
    json["speed"] = m_speed;

    return json;
}
void BonusEntity::loadJson(const Json& json)
{
    setPosition(Vector2f(json["position"][0], json["position"][1]));
    int type = json["type"];
    m_type = static_cast<BonusEntity::Type>(type);
    m_speed = json["speed"];
}