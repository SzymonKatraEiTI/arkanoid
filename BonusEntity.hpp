#ifndef BONUSENTITY_HPP_
#define BONUSENTITY_HPP_

#include <SFML/Graphics.hpp>
#include "Game.hpp"
#include "Scene.hpp"
#include "Entity.hpp"
#include "json.hpp"

using Json = nlohmann::json;
using namespace std;
using namespace sf;

class BonusEntity : public Entity
{
    public:
    enum class Type
    {
        DuplicateBall,
        ExpandPalette,
        CollapsePalette,
        Bullets,
        Count
    };

    static const int MIN_SPEED = 100;
    static const int MAX_SPEED = 150;
    static const int SIZE = 40;
    static const int PALETTE_RESIZE_PIXELS = 40;
    static constexpr float EXPAND_PALETTE_CHANCE = 0.3f;
    static constexpr float COLLAPSE_PALETTE_CHANCE = 0.3f;
    static constexpr float DUPLICATE_BALL_CHANCE = 0.2f;
    static constexpr float BULLETS_CHANCE = 0.2f;

    private:
    RectangleShape m_backShape;
    Type m_type;
    int m_speed;
    FloatRect m_bounds;
    RenderStates m_states;

    static VertexArray s_expandPalettePattern;
    static VertexArray s_collapsePalettePattern;
    static CircleShape s_duplicateBallPattern1;
    static CircleShape s_duplicateBallPattern2;
    static RectangleShape s_bulletsPattern1;
    static RectangleShape s_bulletsPattern2;
    static bool s_patternsInitialized;

    public:
    BonusEntity(Scene& owner);
    BonusEntity(Scene& owner, Type type);

    virtual void initialize();
    virtual const Vector2f& getPosition() const;
    virtual void setPosition(const Vector2f& value);
    virtual void processEvent(const Event& event);
    virtual void update(float delta);
    virtual void render();
    const FloatRect& getBounds();
    Type getType() const;

    virtual Json toJson() const;
    virtual void loadJson(const Json& json);
};

#endif