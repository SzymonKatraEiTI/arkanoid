#include "BallEntity.hpp"

#include <cmath>
#include <cfloat>

BallEntity::BallEntity(Scene& owner) :
    BallEntity(owner, Vector2f(0, 0))
{

}
BallEntity::BallEntity(Scene& owner, const Vector2f& initialPosition, bool stopped) :
    Entity::Entity(owner)
{
    m_shape.setPosition(initialPosition);
    m_stopped = stopped;
}

void BallEntity::initialize()
{
    m_justCollided = false;

    m_shape.setRadius(BallEntity::RADIUS);
    FloatRect bounds = m_shape.getLocalBounds();
    m_shape.setOrigin(bounds.width / 2, bounds.height / 2); // center origin

    m_speed = BallEntity::SPEED;
    m_direction = M_PI / 2;
    computeVelocity();

    m_shape.setFillColor(Color::Red);

    m_previousBounds = m_shape.getGlobalBounds();
}
void BallEntity::processEvent(const Event& event)
{
    if (event.type == Event::MouseButtonPressed)
    {
        if (event.mouseButton.button == Mouse::Button::Left) start();
    }
}
void BallEntity::update(float delta)
{
    m_previousBounds = m_shape.getGlobalBounds();
 
    if (!m_stopped)
    {
        if (m_justCollided)
        {
            setPosition(m_collidePosition);

            m_justCollided = false;
        }
        else
        {
            Vector2f simPos = simulateNextPosition(delta);
            m_shape.setPosition(simPos);
        }
    }
}
void BallEntity::render()
{
    getScene().getGame().getWindow().draw(m_shape);
}

const Vector2f& BallEntity::getPosition() const
{
    return m_shape.getPosition();
}
void BallEntity::setPosition(const Vector2f& value)
{
    m_shape.setPosition(value);
}
bool BallEntity::isStopped() const
{
    return m_stopped;
}
void BallEntity::start()
{
    m_stopped = false;
}
float BallEntity::getSpeed() const
{
    return m_speed;
}
float BallEntity::getDirection() const
{
    return m_direction;
}
const Vector2f& BallEntity::getVelocity() const
{
    return m_velocity;
}
void BallEntity::setSpeedDirection(float speed, float direction)
{
    m_speed = speed;
    m_direction = direction;

    computeVelocity();
}

void BallEntity::bounceX()
{
    m_velocity.x *= -1;
    computeDirection();
}
void BallEntity::bounceY()
{
    m_velocity.y *= -1;
    computeDirection();
}
void BallEntity::bounce(CollisionType type)
{
    if (type == CollisionType::Top || type == CollisionType::Bottom) bounceY();
    else if (type == CollisionType::Left || type == CollisionType::Right) bounceX();
}
void BallEntity::bounce(CollisionType type, float directionChanger)
{
    if (type == CollisionType::Top)
    {
        m_direction = (M_PI / 2) - directionChanger * BallEntity::MAX_BOUNCE_ANGLE;
        computeVelocity();
    }
    else bounce(type);
}

void BallEntity::setCollidePosition(const Vector2f& value)
{
    m_justCollided = true;
    m_collidePosition = value;
}

Vector2f BallEntity::simulateNextPosition(float delta) const
{
    Vector2f pos = m_shape.getPosition();
    pos += m_velocity * delta;
    return pos;
}

void BallEntity::checkCollision(const FloatRect& bounds, float delta, CollisionData& data)
{
    const Vector2f pos = getPosition();
    const Vector2f vel = getVelocity();
    float rad = getRadius();

    // we assume that ball is square of size equal to 2 times of ball radius
    // we check only three collision points, depending on velocity of the ball
    // if ball is moving into top-left corner, then we have to check top-right, top-left and bottom-left corners
    // same for other directions
    
    // relative positions of corners, depending on center of the ball
    // r1 and r2 are always top corners in top movement and bottom corners in bottom movement
    if (vel.x < 0)
    {
        if (vel.y < 0) // top-left movement
        {
            data.r1 = Vector2f(rad, -rad);
            data.r2 = Vector2f(-rad, -rad);
            data.r3 = Vector2f(-rad, rad);
        }
        else // bottom-left movement
        {
            data.r1 = Vector2f(-rad, rad);
            data.r2 = Vector2f(rad, rad);
            data.r3 = Vector2f(-rad, -rad);
        }
    }
    else
    {
        if (vel.y < 0) // top-right movement
        {
            data.r1 = Vector2f(-rad, -rad);
            data.r2 = Vector2f(rad, -rad);
            data.r3 = Vector2f(rad, rad);
        }
        else // bottom-right movement
        {
            data.r1 = Vector2f(-rad, rad);
            data.r2 = Vector2f(rad, rad);
            data.r3 = Vector2f(rad, -rad);
        }
    }
    
    // real positions of the corners
    data.a1 = pos + data.r1;
    data.b1 = pos + data.r2;
    data.c1 = pos + data.r3;

    // predicted real positions of the corners
    data.a2 = data.a1 + vel * delta;
    data.b2 = data.b1 + vel * delta;
    data.c2 = data.c1 + vel * delta;

    // after predictions we have to check these three lines if they are not intersecting with our obstacle ("bounds" paremeter)
    data.t1 = MathUtils::LineRectangleCollision(data.a1, data.a2, bounds, data.d1, data.i1);
    data.t2 = MathUtils::LineRectangleCollision(data.b1, data.b2, bounds, data.d2, data.i2);
    data.t3 = MathUtils::LineRectangleCollision(data.c1, data.c2, bounds, data.d3, data.i3);

    data.Distance = FLT_MAX;
    data.Type = CollisionType::None;

    // here we check if line is colliding and if yes we check if this line has closest distance to the obstacle
    // therfore we are choosing the closest one
    if (data.t1 != CollisionType::None && data.d1 < data.Distance)
    {
        data.Distance = data.d1;
        data.IntersectionPoint = data.i1; 
        data.Type = data.t1;
        data.Relative = data.r1;
    }
    if (data.t2 != CollisionType::None && data.d2 < data.Distance)
    {
        data.Distance = data.d2;
        data.IntersectionPoint = data.i2; 
        data.Type = data.t2;
        data.Relative = data.r2;
    }
    if (data.t3 != CollisionType::None && data.d3 < data.Distance)
    {
        data.Distance = data.d3;
        data.IntersectionPoint = data.i3;
        data.Type = data.t3;
        data.Relative = data.r3;
    }

    data.ObstacleBounds = bounds;
}

void BallEntity::processCollision(const CollisionData& data, float& remainingDelta, CollisionType& previousType, bool paletteBounce)
{
    // we have choosen closest collision and now we can handle it 

    if (data.Distance < 0.01f) // to avoid situations where ball got stuck, just give bounce on another axis to get away from this place
    {
        if (previousType == CollisionType::Top || previousType == CollisionType::Bottom) bounceX();
        if (previousType == CollisionType::Left || previousType == CollisionType::Right) bounceY();

        update(remainingDelta);
        remainingDelta = 0;
    }
    else
    {
        // intersectionPoint is the point where collision appears!
        // we have to take this relative position of the corner because origin of ball is in center of the ball
        setCollidePosition(data.IntersectionPoint - data.Relative);

        float fullDistance = MathUtils::Distance(data.a1, data.a2); // no matter if we choose a, b or c. all of them will give the same distance
        float usedTime = (data.Distance / fullDistance) * remainingDelta;

        update(usedTime);

        remainingDelta -= usedTime;

        if (paletteBounce)
        {
            // when bouncing from palette we have to choose point between two bottom intersections, not the closest one
            Vector2f leftIntersection(data.ObstacleBounds.top, data.ObstacleBounds.left);
            Vector2f rightIntersection(data.ObstacleBounds.top, data.ObstacleBounds.left + data.ObstacleBounds.width);
            // i1 and i2 are bottom corners in this case (see comment next to definition of "Vector2f r1, r2, r3;")
            // i1 - left one, i2 - right one
            if (data.t1 == CollisionType::Top) leftIntersection = data.i1;
            if (data.t2 == CollisionType::Top) rightIntersection = data.i2;
                
            float middlePoint = (leftIntersection.x + rightIntersection.x) / 2.0f;

            float relative = (middlePoint - data.ObstacleBounds.left) / data.ObstacleBounds.width;
            bounce(data.Type, (relative - 0.5f) * 2.0f);
        }
        else
        {
            bounce(data.Type);
        }
    }

    previousType = data.Type;
}

void BallEntity::computeVelocity()
{
    m_velocity.x = static_cast<float>(cos(m_direction) * m_speed);
    m_velocity.y = -static_cast<float>(sin(m_direction) * m_speed);
}
void BallEntity::computeDirection()
{
    m_direction = atan2(-m_velocity.y, m_velocity.x);
}

float BallEntity::getRadius() const
{
    return BallEntity::RADIUS;
}

const FloatRect& BallEntity::getBounds()
{
    m_bounds = m_shape.getGlobalBounds();
    return m_bounds;
}

Json BallEntity::toJson() const
{
    const Vector2f& pos = getPosition();

    Json json;

    json["position"][0] = pos.x;
    json["position"][1] = pos.y;
    json["speed"] = m_speed;
    json["direction"] = m_direction;
    json["stopped"] = m_stopped;

    return json;
}
void BallEntity::loadJson(const Json& json)
{
    setPosition(Vector2f(json["position"][0], json["position"][1]));
    setSpeedDirection(json["speed"], json["direction"]);
    m_stopped = json["stopped"];
}