#ifndef SCENE_HPP_
#define SCENE_HPP_

#include <vector>
#include <memory>
#include <SFML/Window.hpp>
#include "Entity.hpp"

using namespace std;
using namespace sf;

class Game;

class Scene
{
private:
    Game& m_owner;
    vector<shared_ptr<Entity>> m_entities;
    bool m_initialized;

public:
    Scene(Game& owner);

    virtual void initialize();
    Game& getGame();
    virtual void processEvent(const Event& event) = 0;
    virtual void update(float delta) = 0;
    virtual void render() = 0;

    bool isInitialized() const;
};

#endif // SCENE_HPP_
