#ifndef PALETTE_HPP_
#define PALETTE_HPP_

#include <SFML/Graphics.hpp>
#include "Scene.hpp"
#include "Entity.hpp"
#include "Game.hpp"
#include "json.hpp"

using Json = nlohmann::json;
using namespace sf;

class PaletteEntity : public Entity
{
private:
    static const int DEFAULT_WIDTH = 150;
    static const int MIN_WIDTH = 30;
    static const int MAX_WIDTH = 270;
    static const int HEIGHT = 20;
    static const int CANNON_WIDTH = 10;

    static RectangleShape s_cannon;
    static bool s_cannonInitialized;
    RenderStates m_leftCannonStates;
    RenderStates m_rightCannonStates;
    RectangleShape m_shape;
    FloatRect m_bounds;
    int m_width;
    bool m_hasCannons;
    float m_cannonCooldownTime;

public:
    static constexpr float CANNON_COOLDOWN_TIME = 0.7f;

    PaletteEntity(Scene& owner);

    virtual void initialize();
    virtual const Vector2f& getPosition() const;
    virtual void setPosition(const Vector2f& value);
    virtual void processEvent(const Event& event);
    virtual void update(float delta);
    virtual void render();
    const FloatRect& getBounds();
    void setWidth(int value);
    int getWidth() const;
    void setCannons(bool value);
    bool areCannonsAvailable() const;
    bool hasCannons() const;
    void applyCannonsCooldown();

    virtual Json toJson() const;
    virtual void loadJson(const Json& json);
};

#endif // PALETTE_HPP_