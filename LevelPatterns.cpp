#include "LevelPatterns.hpp"

#include <fstream>
#include "json.hpp"
#include "Game.hpp"
#include <windows.h>

using Json = nlohmann::json;

LevelPatterns::LevelPatterns()
{
}

void LevelPatterns::loadFromFile(const string& fileName, vector<FloatRect>& pattern)
{
    try
    {
        ifstream file(fileName);
        Json root;
        file >> root;
        file.close();

        int brickWidth = root["brickWidth"];
        int brickHeight = root["brickHeight"];
        Json bricks = root["bricks"];

        for (Json& coords : bricks)
        {
            pattern.push_back(FloatRect(coords[0], coords[1], brickWidth, brickHeight));
        }
    }
    catch (...)
    {
        string msg = "An error occured while loading level from " + fileName;
        MessageBox(NULL, msg.c_str(), NULL, MB_OK | MB_ICONEXCLAMATION);
    }
}
void LevelPatterns::loadLevel(int level, vector<FloatRect>& pattern)
{
    loadFromFile(getLevelPath(level), pattern);
}
bool LevelPatterns::isLevelExists(int level)
{
    DWORD dwAttrib = GetFileAttributes(getLevelPath(level).c_str());

    return (dwAttrib != INVALID_FILE_ATTRIBUTES && 
         !(dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
}
string LevelPatterns::getLevelPath(int level)
{
    return "levels/level" + to_string(level) + ".json";
}