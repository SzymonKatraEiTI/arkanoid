#ifndef SINUSMOVEMENTBEHAVIOUR_HPP_
#define SINUSMOVEMENTBEHAVIOUR_HPP_

#include <SFML/Graphics.hpp>
#include "MovementBehaviour.hpp"

class SinusMovementBehaviour : public MovementBehaviour
{
    protected:
    virtual float movementFunction(float x) const;

    public:
    SinusMovementBehaviour();

    virtual const string& getUniqueName() const;

    static const string UNIQUE_NAME;
};

#endif // SINUSMOVEMENTBEHAVIOUR_HPP_