#include "Entity.hpp"

Entity::Entity(Scene& owner) :
    m_owner(owner),
    m_initialized(false)
{

}

void Entity::initialize()
{
    m_initialized = true;
}
Scene& Entity::getScene()
{
    return m_owner;
}
bool Entity::isInitialized() const
{
    return m_initialized;
}