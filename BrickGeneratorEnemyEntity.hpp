#ifndef BRICKGENERATORENEMYENTITY_HPP_
#define BRICKGENERATORENEMYENTITY_HPP_

#include <SFML/Graphics.hpp>
#include "EnemyEntity.hpp"
#include "Entity.hpp"
#include "Scene.hpp"
#include "Game.hpp"
#include "json.hpp"
#include "MathUtils.hpp"
#include "MovementBehaviour.hpp"

using Json = nlohmann::json;
using namespace std;
using namespace sf;

class BrickGeneratorEnemyEntity : public EnemyEntity
{
    private:
    float m_newBrickChance;
    float m_newBrickInterval;
    float m_newBrickIntervalCounter;    

    public:
    static const string UNIQUE_NAME;
    static constexpr float DEFAULT_NEW_BRICK_CHANCE = 0.003f;
    static constexpr float DEFAULT_NEW_BRICK_INTERVAL = 1.0f;

    BrickGeneratorEnemyEntity(Scene& owner, shared_ptr<MovementBehaviour> behaviour = nullptr, int size = EnemyEntity::DEFAULT_SIZE);

    virtual void initialize();
    virtual void update(float delta);

    bool newBrickRandom();

    float getNewBrickChance() const;
    void setNewBrickChance(float value);
    float getNewBrickInterval() const;
    void setNewBrickInterval(float value);

    virtual const string& getUniqueName() const;

    virtual Json toJson() const;
    virtual void loadJson(const Json& json);
};

#endif // BRICKGENERATORENEMYENTITY_HPP_