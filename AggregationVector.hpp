#ifndef AGGREGATIONVECTOR_HPP_
#define AGGREGATIONVECTOR_HPP_

#include <vector>
#include <tuple>
#include <algorithm>
#include <memory>

using namespace std;

template<typename T>
class AggregationVector : public vector<T>
{
    private:
    template<typename A>
    tuple<A> getSingle(int index)
    {
        for (typename vector<T>::iterator i = vector<T>::begin(); i != vector<T>::end(); i++)
        {
            A element = dynamic_cast<A>(*i);
            if (element != nullptr && index-- <= 0) return make_tuple(element);
        }

        return make_tuple(nullptr);
    }

    public:
    template<typename... An>
    tuple<An...> get(int index = 0)
    {
        return tuple_cat(getSingle<An>(index)...);
    }
};


template<typename T>
class SharedPtrAggregationVector : public vector<shared_ptr<T>>
{
    public:
    template<typename A>
    tuple<shared_ptr<A>> getSingle(int index)
    {
        for (typename vector<shared_ptr<T>>::iterator i = vector<shared_ptr<T>>::begin(); i != vector<shared_ptr<T>>::end(); i++)
        {
            shared_ptr<A> element = dynamic_pointer_cast<A>(*i);
            if (element != nullptr && index-- <= 0) return make_tuple(element);
        }

        return make_tuple(nullptr);
    }

    template<typename... An>
    tuple<shared_ptr<An>...> get(int index = 0)
    {
        return tuple_cat(getSingle<An>(index)...);
    }
};

#endif // AGGREGATIONVECTOR_HPP_