#include "AggregationVectorTest.hpp"

#include <iostream>
#include <tuple>
#include "AggregationVector.hpp"

using namespace std;

void AggregationVectorTest::runAll()
{
    Base a1(1);
    Base a2(2);
    Derived1 b3(3);
    DoubleDerived1 bb4(4);
    Derived2 c5(5);
    Derived2 c6(6);

    AggregationVector<Base*> vec;
    vec.push_back(&a1);
    vec.push_back(&a2);
    vec.push_back(&b3);
    vec.push_back(&bb4);
    vec.push_back(&c5);
    vec.push_back(&c6);

    tuple<Base*, DoubleDerived1*> tup1 = vec.get<Base*, DoubleDerived1*>();
    tuple<Derived1*, DoubleDerived1*, Derived2*> tup2 = vec.get<Derived1*, DoubleDerived1*, Derived2*>();

    cout << "Test Tuple1 result: " << (get<0>(tup1)->Value == 1 && get<1>(tup1)->Value == 4) << endl;
    cout << "Test Tuple2 result: " << (get<0>(tup2)->Value == 3 && get<1>(tup2)->Value == 4 && get<2>(tup2)->Value == 5) << endl;
}