#ifndef NORMALENEMYENTITY_HPP_
#define NORMALENEMYENTITY_HPP_

#include "EnemyEntity.hpp"
#include <string>

using namespace std;

class NormalEnemyEntity : public EnemyEntity
{
    public:
    static const string UNIQUE_NAME;
    NormalEnemyEntity(Scene& owner, shared_ptr<MovementBehaviour> behaviour = nullptr, int size = EnemyEntity::DEFAULT_SIZE);

    virtual const string& getUniqueName() const;
};

#endif // NORMALENEMYENTITY_HPP_