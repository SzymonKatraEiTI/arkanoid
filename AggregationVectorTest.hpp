#ifndef AGGREGATIONVECTORTEST_HPP_
#define AGGREGATIONVECTORTEST_HPP_

#include <iostream>

using namespace std;

class AggregationVectorTest
{
    private:
    class Base
    {
        public:
        int Value;

        Base(int v = 0) { Value = v; }

        virtual void write()
        {
            cout << "Base " << Value << endl;
        }

        virtual ~Base() { }
    };
    class Derived1 : public Base
    {
        public:
        Derived1(int v = 0) : Base::Base(v) { }

        virtual void write()
        {
            cout << "Derived1 " << Value << endl;
        }
    };
    class DoubleDerived1 : public Derived1
    {
        public:
        DoubleDerived1(int v = 0) : Derived1::Derived1(v) { }

        virtual void write()
        {
            cout << "DoubleDerived1 " << Value << endl;
        }
    };
    class Derived2 : public Base
    {
        public:
        Derived2(int v = 0) : Base::Base(v) { }

        virtual void write()
        {
            cout << "Derived2 " << Value << endl;
        }
    };

    public:
    static void runAll();
};

#endif // AGGREGATIONVECTORTEST_HPP_