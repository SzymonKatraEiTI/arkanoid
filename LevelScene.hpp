#ifndef LEVELSCENE_HPP_
#define LEVELSCENE_HPP_

#include <vector>
#include <memory>
#include "Game.hpp"
#include "Scene.hpp"
#include "PaletteEntity.hpp"
#include "BlockEntity.hpp"
#include "BallEntity.hpp"
#include "BrickEntity.hpp"
#include "CollisionType.hpp"
#include "BonusEntity.hpp"
#include "EnemyEntity.hpp"
#include "AggregationVector.hpp"
#include <tuple>

using namespace std;
using namespace sf;

class LevelScene : public Scene
{
private:
    int m_level;
    int m_chances;
    shared_ptr<PaletteEntity> m_palette;
    vector<shared_ptr<BallEntity>> m_balls;
    vector<shared_ptr<BlockEntity>> m_walls;
    vector<shared_ptr<BrickEntity>> m_bricks;
    vector<shared_ptr<BonusEntity>> m_bonuses;
    SharedPtrAggregationVector<EnemyEntity> m_enemies;

    RectangleShape m_leftBullet;
    RectangleShape m_rightBullet;
    float m_remainingBulletVisibilityTime;
    Font m_chancesLeftFont;
    Text m_chancesLeftText;
    bool m_paused;

    bool changeEnemyBehaviour(shared_ptr<EnemyEntity> enemy, shared_ptr<MovementBehaviour> behaviour, int index, float progressMultiplier = 0.03f);
    void checkBallCollisions(float delta, shared_ptr<BallEntity> ball);
    void checkBulletsCollisions();
    void processBulletCollision(const Vector2f& start, const Vector2f& end, float& closestDistance, Vector2f& closestIntersectionPoint);
    void checkBonusesCollisions(float delta);
    void handleRemovingBrick(shared_ptr<BrickEntity> brick);
    void handleRemovingEnemy(shared_ptr<EnemyEntity> enemy);
    void processEnemys(float delta);
    bool canProceedToNextLevel() const;
    bool isGameLose() const;

public:
    static constexpr float BONUS_CHANCE = 0.15f;
    static constexpr float BULLET_VISIBILITY_TIME = 0.15f;

    LevelScene(Game& owner, int level = 0, int chances = 4);

    virtual void initialize();
    virtual void processEvent(const Event& event);
    virtual void update(float delta);
    virtual void render();

    void saveStateToFile(const string& fileName);
    void loadStateFromFile(const string& fileName);
};

#endif // LEVELSCENE_HPP_
