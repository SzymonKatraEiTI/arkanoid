#include "Game.hpp"

#include <SFML/Window.hpp>

Game::Game() :
    m_window(VideoMode(Game::WIDTH, Game::HEIGHT), "arkanoid", Style::Default, ContextSettings(0, 0, 4)),
    m_scene(nullptr),
    m_newScene(nullptr)
{
    m_window.setFramerateLimit(60);
}

void Game::run()
{
    Clock clock;

    while (m_window.isOpen())
    {
        if (m_newScene != nullptr)
        {
            m_scene = m_newScene;
            m_newScene = nullptr;
        }
        

        if (m_scene != nullptr)
        {
            Time time = clock.restart();

            Event event;
            while (m_window.pollEvent(event))
            {
                if (event.type == Event::Closed) m_window.close();
                m_scene->processEvent(event);
            }       

            m_scene->update(time.asSeconds());

            m_window.clear(Color::White);
            m_scene->render();
            m_window.display();
        }
    }
}

void Game::setScene(shared_ptr<Scene> scene)
{
    if (!scene->isInitialized()) scene->initialize();
    m_newScene = scene;
}
shared_ptr<Scene> Game::getScene()
{
    return m_scene;
}
RenderWindow& Game::getWindow()
{
    return m_window;
}