#include "BrickGeneratorEnemyEntity.hpp"

#include <cstdlib>

const string BrickGeneratorEnemyEntity::UNIQUE_NAME("brickgenerator");

BrickGeneratorEnemyEntity::BrickGeneratorEnemyEntity(Scene& owner, shared_ptr<MovementBehaviour> behaviour, int size) :
    EnemyEntity::EnemyEntity(owner, behaviour, size)
{
}

void BrickGeneratorEnemyEntity::initialize()
{
    EnemyEntity::initialize();

    m_newBrickInterval = DEFAULT_NEW_BRICK_INTERVAL;
    m_newBrickChance = DEFAULT_NEW_BRICK_CHANCE;
    m_newBrickIntervalCounter = m_newBrickInterval;

    m_shape.setFillColor(Color::Cyan);
}
void BrickGeneratorEnemyEntity::update(float delta)
{
    EnemyEntity::update(delta);

    m_newBrickIntervalCounter -= m_newBrickInterval * delta;
}

bool BrickGeneratorEnemyEntity::newBrickRandom()
{
    if (isStopped()) return false;
    if (m_newBrickIntervalCounter >= 0.0f) return false;

    if (MathUtils::RandomFloat() <= m_newBrickChance)
    {
        m_newBrickInterval = m_newBrickInterval;
        return true;
    }

    return false;
}

float BrickGeneratorEnemyEntity::getNewBrickChance() const
{
    return m_newBrickChance;
}
void BrickGeneratorEnemyEntity::setNewBrickChance(float value)
{
    m_newBrickChance = value;
}
float BrickGeneratorEnemyEntity::getNewBrickInterval() const
{
    return m_newBrickInterval;
}
void BrickGeneratorEnemyEntity::setNewBrickInterval(float value)
{
    m_newBrickInterval = value;
}

const string& BrickGeneratorEnemyEntity::getUniqueName() const
{
    return BrickGeneratorEnemyEntity::UNIQUE_NAME;
}

Json BrickGeneratorEnemyEntity::toJson() const
{
    Json json = EnemyEntity::toJson();

    json["newBrickChance"] = m_newBrickChance;
    json["newBrickInterval"] = m_newBrickInterval;
    json["newBrickIntervalCounter"] = m_newBrickIntervalCounter;

    return json;
}
void BrickGeneratorEnemyEntity::loadJson(const Json& json)
{
    EnemyEntity::loadJson(json);

    m_newBrickChance = json["newBrickChance"];
    m_newBrickInterval = json["newBrickInterval"];
    m_newBrickIntervalCounter = json["newBrickIntervalCounter"];
}